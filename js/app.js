
// faq 

const $faqBlock = document.querySelector('.faq-column');

if ($faqBlock) {
    $faqBlock.addEventListener('click', e => {
        e.preventDefault();
        const $target = e.target;
        const $faqItem = $target.closest('.faq-column .item');
        const $faqItemTop = $target.closest('.item-title');
        const $faqItemAll = $faqBlock.querySelectorAll('.faq-column .item');
    
        if ($faqItem) {
            if ($faqItem.getAttribute('active') && $faqItemTop) {
    
                $faqItemAll.forEach(item => {
                    item.classList.remove('active');
                    item.removeAttribute('active');
                });
                $faqItem.classList.remove('active');
                $faqItem.removeAttribute('active');
            } else {
                
                $faqItemAll.forEach(item => {
                    item.classList.remove('active');
                    item.removeAttribute('active');
                });
                $faqItem.setAttribute('active', 'active');
                $faqItem.classList.add('active');
               
            }
            
        } 
    
    });
    
}

// menu

const v_headerMenuM = document.querySelector('.header-menu-m'),
v_modalMenu = document.querySelector('.modal-menu'),
v_modalMenuClose = document.querySelector('.modal-menu-close');

if (v_headerMenuM) {
    v_headerMenuM.addEventListener('click', () => {
        v_modalMenu.classList.add('active');
    });
    v_modalMenuClose.addEventListener('click', () => {
        v_modalMenu.classList.remove('active');
    });
}



// показать весь текст

const $boxVisDescp = document.querySelectorAll('.box-vis-descp');

if ($boxVisDescp) {
    $boxVisDescp.forEach(item => {

        const $read = item.querySelector('.read-more');
        const $subtext = item.querySelectorAll('.subtitle');

        $subtext.forEach(item => {
            item.style.display = 'none';
            $subtext[0].style.display = '';
        })
        if ($read) {
            $read.addEventListener('click', e => {
                e.preventDefault();
                $subtext[0].style.display = '';
                $subtext.forEach(item => {
             
                    if (item.getAttribute('active')) {
    
                        item.style.display = 'none';
                        $subtext[0].style.display = '';
                        item.removeAttribute('active');
                        item.classList.remove('active');
    
                    } else {
                        item.style.display = '';
                        
                        item.classList.add('active');
                        item.setAttribute('active', 'active');
    
                    }
                })
            });
        }
      
    });
}


// таймер

const timerShowHour = document.querySelector('.timer-js .hour ');
const timerShowMinuts = document.querySelector('.timer-js .minuts ');
const timerShowSeconds = document.querySelector('.timer-js .seconds');
let timeMinut = 48 * 60 * 10;

if (timerShowHour && timerShowMinuts && timerShowSeconds ) {

    window.onload = function() {

        timer = setInterval(function () {
            seconds = timeMinut%60
            minutes = timeMinut/60%60
            hour = timeMinut/60/60%60
            if (!timeMinut <= 0) {
                let strTimerHour = `${Math.trunc(hour)}`;
                let strTimerMinuts = `${Math.trunc(minutes)}`;
                let strTimerSeconds = `${Math.trunc(seconds)}`;
                
                timerShowHour.textContent = strTimerHour;
                timerShowMinuts.textContent = strTimerMinuts;
                timerShowSeconds.textContent = strTimerSeconds;
            
            }
            --timeMinut;
            
        }, 1000);
    };
}
